// Define a template. It’s just a function that returns a template literal string.
module.exports = (port) =>
  `<p class="server">Server is on: ${port}</p>`;
